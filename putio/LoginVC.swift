//
//  LoginVC.swift
//  putio
//
//  Created by Javi Manzano on 01/11/2015.
//  Copyright © 2015 Javi Manzano. All rights reserved.
//

import UIKit
import ReachabilitySwift

class LoginVC: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webView: UIWebView!
    
    let fileListSegueId: String = "showFiles"
    let mainViewControllerId = "MainViewController"
    let offlineViewControllerId = "OfflineViewController"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        webView.delegate = self
        
        self.navigationItem.title = "Login put.io"
        
        let reachability = Reachability()!
        
        reachability.whenReachable = { reachability in
            // this is called on a background thread, but UI updates must
            // be on the main thread, like this:
            DispatchQueue.main.async {
                let url : URL! = URL(string: "https://api.put.io/v2/oauth2/authenticate?client_id=2187&response_type=token&redirect_uri=https://putio-javiman.herokuapp.com/")
                self.webView.loadRequest(URLRequest(url: url))
            }
        }
        reachability.whenUnreachable = { reachability in
            // this is called on a background thread, but UI updates must
            // be on the main thread, like this:
            DispatchQueue.main.async {
                // Take user to downloads view
                let offlineViewController = self.storyboard?.instantiateViewController(withIdentifier: self.offlineViewControllerId) as! UINavigationController
                self.present(offlineViewController, animated: false, completion: nil)
            }
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if request.url != nil {
            let url = String(describing: request.url!)
            if url.contains("https://putio-javiman.herokuapp.com/#access_token=") {
                
                // Get the access token from the url.
                let accessToken = url.replacingOccurrences(of: "https://putio-javiman.herokuapp.com/#access_token=", with: "")
                
                // Store it in the standard preferences.
                UserDefaults.standard.set(accessToken, forKey: "accessToken")
                
                // Open the main navigation controller.
                let navigationController = self.storyboard?.instantiateViewController(withIdentifier: mainViewControllerId) as! UITabBarController
                self.present(navigationController, animated: false, completion: nil)
            }
        }
        return true
    }
    
}

