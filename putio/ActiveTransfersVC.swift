//
//  ActiveTransfersVC.swift
//  putio
//
//  Created by Javi Manzano on 08/09/2016.
//  Copyright © 2016 Javi Manzano. All rights reserved.
//

import UIKit

class ActiveTransfersVC: UITableViewController {
    
    // Ids
    let transferCellId = "TransferCell"
    let viewControllerId = "FileListViewController"
    
    // Fetch task
    var putioFilesController = PutioFilesController.sharedInstance
    
    // List of active transfers.
    var transferList: [File] = []
    
    // Define a spinner
    var activityIndicator: UIActivityIndicatorView?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Set table cells automatic height
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.estimatedRowHeight = 64
        tableView.rowHeight = UITableViewAutomaticDimension
        
        // Initialize spinner
        activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0,y: 0, width: 16, height: 16))
        activityIndicator!.hidesWhenStopped = true
        activityIndicator!.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        navigationItem.rightBarButtonItems?.append(UIBarButtonItem(customView: activityIndicator!))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        fetchActiveTransfers()
    }
    
    func fetchActiveTransfers () {
        activityIndicator!.startAnimating()
        
        putioFilesController.getActiveTransfers(({ (files) in
            self.activityIndicator!.stopAnimating()
            self.transferList = files
            self.tableView.reloadData()
        }))
    }

}


extension ActiveTransfersVC {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transferList.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let indexPath = tableView.indexPathForSelectedRow as IndexPath! {
            
            let file = transferList[(indexPath as NSIndexPath).row]
            
            // TODO: Do this properly. The bottom tab bar should select the put.io options.
            let vc = storyboard?.instantiateViewController(withIdentifier: viewControllerId) as! FileListVC
            vc.parentFile = File(
                id: file.getParentId(),
                name: "SOMETHING",
                parentId: 0,
                thumbnail: "",
                contentType: "",
                createdAt: "",
                hasMp4: false,
                size: 0,
                fileExtension: "",
                finished: true,
                accessToken: "")
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: transferCellId, for: indexPath)
        
        // Retrieve the story with the cell index
        let file = transferList[(indexPath as NSIndexPath).row]
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.text = "\(file.getName())"
        
        if file.finished == true {
            // Green if done
            cell.backgroundColor = UIColor(colorLiteralRed: 204 / 255, green: 255 / 255, blue: 102 / 255, alpha: 1.0)
        } else {
            // Yellow if in progress
            cell.backgroundColor = UIColor(colorLiteralRed: 255 / 255, green: 247 / 255, blue: 157 / 255, alpha: 1.0)
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.isUserInteractionEnabled = false
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let file = transferList[(indexPath as NSIndexPath).row]
            
            // Cancel transfer
            print("Cancelling transfer for \(file.getName())")
            transferList.remove(at: (indexPath as NSIndexPath).row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
}
