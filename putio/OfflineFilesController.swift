//
//  OfflineFilesController.swift
//  putio
//
//  Created by Javi Manzano on 04/09/2016.
//  Copyright © 2016 Javi Manzano. All rights reserved.
//

import Foundation

class OfflineFilesController {
    
    /*
     Return a list of offline files in the default documents directory.
    */
    func getOfflineFiles() -> [File] {
        var fileList: [File] = []
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsURL = paths[0]
        
        do {
            let fileManager = FileManager.default
            let urlList = try fileManager.contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: nil, options: .skipsPackageDescendants)
            for url in urlList {
                
                let fileName = url.path.characters.split{$0 == "/"}.map(String.init).last
                
                // Only support mp4 files for offline usage.
                if (fileName!.contains("mp4")) {
                    fileList.append(
                        File(
                            id: 0,
                            name: fileName,
                            parentId: -1,
                            thumbnail: nil,
                            contentType: "video",
                            createdAt: nil,
                            hasMp4: true,
                            size: -1,
                            fileExtension: "mp4",
                            finished: true,
                            accessToken: ""
                    ))
                }
            }
        } catch {
            print("Couldn't get the list of local files")
        }
        
        return fileList
    }
    
    /*
     Delete a given file from the offline documents directory.
    */
    func deleteOfflineFile (file: File) {
        do {
            try FileManager.default.removeItem(at: getFileOfflineURL(file: file))
            print("Deleted offline file")
        } catch {
            print("There was an error deleting \(file.getOfflineFileName())")
        }
    }
    
    /*
     Checks whether a file is offline or not.
    */
    func isFileOffline(file: File) -> Bool {
        return FileManager.default.fileExists(atPath: getFileOfflineURL(file: file).path)
    }
    
    /*
     Gets a file playback url.
    */
    func getFilePlaybackURL(file: File) -> URL {
        if isFileOffline(file: file) {
            return getFileOfflineURL(file: file)
        }
        return file.getDownloadURL() as URL
    }
    
    /*
     Gets the path for an offline file
    */
    func getFileOfflineURL(file: File) -> URL {
        let fileManager = FileManager.default
        let documentsDirectory = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        return documentsDirectory.appendingPathComponent(file.getOfflineFileName())
    }
}
