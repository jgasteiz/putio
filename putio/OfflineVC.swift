//
//  OfflineVC.swift
//  putio
//
//  Created by Javi Manzano on 31/10/2015.
//  Copyright © 2015 Javi Manzano. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class OfflineVC: UITableViewController {
    
    // Ids
    let cellId = "LabelCell"
    
    let offlineFilesController = OfflineFilesController()
    
    // List of files and directories
    var fileList: [File] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set table cells automatic height
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "LabelCell")
        tableView.estimatedRowHeight = 64
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Fetch the files and directories in the current directory.
        fileList = offlineFilesController.getOfflineFiles()
        tableView.reloadData()
    }
}

extension OfflineVC {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fileList.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let indexPath = self.tableView.indexPathForSelectedRow as IndexPath! {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MediaViewController") as! AVPlayerViewController
            
            vc.player = AVPlayer(url: offlineFilesController.getFilePlaybackURL(file: fileList[(indexPath as NSIndexPath).row]) as URL)
            
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        
        // Retrieve the story with the cell index
        let file = fileList[(indexPath as NSIndexPath).row]
        cell.textLabel?.text = "\(file.getName())"
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let file = fileList[(indexPath as NSIndexPath).row]
            
            // Delete the local file.
            offlineFilesController.deleteOfflineFile(file: file)
            
            // Update the file list and tableview.
            fileList.remove(at: (indexPath as NSIndexPath).row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
}


