//
//  FileListVC.swift
//  putio
//
//  Created by Javi Manzano on 31/10/2015.
//  Copyright © 2015 Javi Manzano. All rights reserved.
//

import UIKit

class FileListVC: UITableViewController {
    
    // MARK: - Properties
    
    // Ids
    let fileCellId = "FileCell"
    let viewControllerId = "FileListViewController"
    let fileDetailViewControllerId = "FileDetailViewController"
    
    // Fetch task
    var putioFilesController = PutioFilesController.sharedInstance
    
    // This won't be null if we're not in the root.
    var parentFile: File?
    
    // List of files and directories
    var fileList: [File] = []
    
    // Define a spinner
    var activityIndicator: UIActivityIndicatorView?

    // MARK: - Override functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if parentFile != nil {
            navigationItem.title = parentFile!.getName()
        } else {
            navigationItem.title = "Put.io viewer"
        }
        
        // Plain back button
        navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        // Set table cells automatic height
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.estimatedRowHeight = 64
        tableView.rowHeight = UITableViewAutomaticDimension
        
        // Initialize spinner
        activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0,y: 0, width: 16, height: 16))
        activityIndicator!.hidesWhenStopped = true
        activityIndicator!.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        navigationItem.rightBarButtonItems?.append(UIBarButtonItem(customView: activityIndicator!))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        fetchDirectoryFiles()
    }
    
    
    
    // MARK: - IBActions
    
    @IBAction func addTransfer(_ sender: AnyObject) {
        addTransfer()
    }
    
    
    
    // MARK: - Functions
    
    /*
     Fetch the files and directories in the current directory.
    */
    func fetchDirectoryFiles () {
        activityIndicator!.startAnimating()
        
        putioFilesController.fetchDirectoryFiles(parentFile, onFilesFetched: ({ (files) in
            self.activityIndicator!.stopAnimating()
            self.fileList = files
            self.tableView.reloadData()
        }))
    }
    
    /*
     Open an alert to ask the user for a link and add it to the put.io transfers.
    */
    func addTransfer () {
        var newLinkTextField = UITextField()
        
        let alertController = UIAlertController(
            title: "New link",
            message: "Paste the download link here",
            preferredStyle: UIAlertControllerStyle.alert
        )
        
        alertController.addTextField { (textField) in
            textField.placeholder = "Magnet link"
            newLinkTextField = textField
        }
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (UIAlertAction) in
            if let downloadURL = newLinkTextField.text {
                self.putioFilesController.addTransfer(downloadURL, onTransferAdded: {
                    print("Success!")
                }, parentId: self.parentFile?.getId())
            }
        }))
        
        present(alertController, animated: true, completion: nil)
    }
    
    /*
     Open an alert to ask the user about deleting a put.io file/directory.
    */
    func deleteFile (_ file: File, indexPath: IndexPath) {
        
        let alertController = UIAlertController(
            title: "Delete file",
            message: "Do you want to delete the file `\(file.getName())`?",
            preferredStyle: UIAlertControllerStyle.alert
        )
        
        alertController.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: nil))
        alertController.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.destructive, handler: { (UIAlertAction) in
            // Delete the remote file.
            self.putioFilesController.deleteRemoteFile(file, onFileDeleted: {
                
                // Update the file list and tableview.
                self.fileList.remove(at: (indexPath as NSIndexPath).row)
                self.tableView.deleteRows(at: [indexPath], with: .automatic)
            })
        }))
        
        present(alertController, animated: true, completion: nil)
    }
}

// MARK: - Extensions

extension FileListVC {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fileList.count;
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let indexPath = tableView.indexPathForSelectedRow as IndexPath! {
            
            let file = fileList[(indexPath as NSIndexPath).row]

            // If it's a directory, open it.
            if file.getContentType() == "application/x-directory" {
                let vc = storyboard?.instantiateViewController(withIdentifier: viewControllerId) as! FileListVC
                vc.parentFile = file
                navigationController?.pushViewController(vc, animated: true)
            } else {
                let vc = storyboard?.instantiateViewController(withIdentifier: fileDetailViewControllerId) as! FileDetailVC
                vc.file = file
                navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Get the table cell
        let cell = tableView.dequeueReusableCell(withIdentifier: fileCellId) as! FileCell
        
        // Retrieve the story with the cell index
        let file = fileList[(indexPath as NSIndexPath).row]
        
        // Set the labels text with the story values
        cell.fileName.text = "\(file.getName())"
        
        cell.fileIconImageView.image = file.getIcon()
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let file = fileList[(indexPath as NSIndexPath).row]
            deleteFile(file, indexPath: indexPath)
        }
    }
}


