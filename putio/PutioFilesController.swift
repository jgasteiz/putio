//
//  PutioFilesController.swift
//  putio
//
//  Created by Javi Manzano on 31/10/2015.
//  Copyright © 2015 Javi Manzano. All rights reserved.
//

import Foundation
import Alamofire

class PutioFilesController {
    
    var downloads = [Int: [String: AnyObject]]()
    
    static let sharedInstance = PutioFilesController()
    
    /*
     Fetch a directory of files from put.io.
     */
    func fetchDirectoryFiles(_ parent: File?, onFilesFetched: @escaping ([File]) -> Void) {
        guard let accessToken = UserDefaults.standard.value(forKey: "accessToken") as? String else {
            print("Access token is missing")
            return
        }
        
        Alamofire.request(
            getApiURL(parent, accessToken: accessToken),
            method: .get
            )
            .responseJSON(completionHandler: { response in
                if let responseJson = response.result.value as? NSDictionary, let filesJson = responseJson["files"] as? [NSDictionary] {
                    // Get the files
                    var files: [File] = []
                    
                    // Add the fetched stories to the array
                    for fileDict in filesJson {
                        files.append(File(
                            id: fileDict["id"] as? Int,
                            name: fileDict["name"] as? String,
                            parentId: fileDict["parent_id"] as? Int,
                            thumbnail: fileDict["screenshot"] as? String,
                            contentType: fileDict["content_type"] as? String,
                            createdAt: fileDict["created_at"] as? String,
                            hasMp4: fileDict["is_mp4_available"] as? Bool,
                            size: fileDict["size"] as? Int,
                            fileExtension: fileDict["extension"] as? String,
                            finished: true,
                            accessToken: accessToken
                            ))
                    }
                    
                    onFilesFetched(files)
                }
            })
    }
    
    /*
     Download a file from put.io to local storage.
     */
    func downloadFile(file: File, downloadURL: URL) -> Void {
        
        downloads[file.getId()] = ["downloadProgress": 0.0 as AnyObject, "taskRunning": true as AnyObject]
        
        let fileDestination = DownloadRequest.suggestedDownloadDestination(for: .documentDirectory)
        
        let download = Alamofire.download(
            downloadURL,
            to: fileDestination
            )
            .downloadProgress { progress in
                DispatchQueue.main.async {
                    self.downloads[file.getId()]!["downloadProgress"] = progress.fractionCompleted as AnyObject
                }
            }
            .responseData { response in
                if response.result.value != nil {
                    print("File downloaded successfully: \(file.getName())")
                } else {
                    print("There was an error downloading the file.")
                }
                self.downloads[file.getId()] = ["downloadProgress": 0.0 as AnyObject, "taskRunning": false as AnyObject]
            }
        
        downloads[file.getId()]!["request"] = download
    }
    
    /*
     Cancel an active request for downloading a file.
     */
    func cancelDownload(_ file: File) {
        if let download = downloads[file.getId()] {
            let request = download["request"] as! Alamofire.Request
            request.cancel()
            downloads[file.getId()] = ["taskRunning": false as AnyObject]
        }
    }
    
    /*
     Delete a remote put.io file or directory.
     */
    func deleteRemoteFile(_ file: File, onFileDeleted: @escaping () -> Void) {
        guard let accessToken = UserDefaults.standard.value(forKey: "accessToken") as? String else {
            print("Access token is missing")
            return
        }
        
        Alamofire.request(
            getDeleteApiURL(accessToken),
            method: .post,
            parameters: ["file_ids": String(file.getId())]
            )
            .response(completionHandler: { response in
                if response.response?.statusCode == 200 {
                    print("File deleted successfully: \(file.getName())")
                    onFileDeleted()
                } else {
                    print("Failed with error: \(response.error)")
                    print(response.data)
                    print(String(describing: response.response))
                }
            })
    }
    
    func getActiveTransfers (_ onTransfersFetched: @escaping ([File]) -> Void) {
        guard let accessToken = UserDefaults.standard.value(forKey: "accessToken") as? String else {
            print("Access token is missing")
            return
        }
        
        Alamofire.request(
            getActiveTransfersApiURL(accessToken),
            method: .get
            )
            .responseJSON(completionHandler: { response in
                print("transfers fetched")
                
                if let responseJson = response.result.value as? NSDictionary, let transfers = responseJson["transfers"] as? [NSDictionary] {
                    var activeTransfers: [File] = []
                    
                    for transfer in transfers {
                        var isFinished = false
                        if let _ = transfer["finished_at"] as? String {
                            isFinished = true
                        }
                        
                        activeTransfers.append(File(
                            id: transfer["id"] as? Int,
                            name: transfer["name"] as? String,
                            parentId: transfer["save_parent_id"] as? Int,
                            thumbnail: "",
                            contentType: "",
                            createdAt: "",
                            hasMp4: false,
                            size: transfer["size"] as? Int,
                            fileExtension: "",
                            finished: isFinished,
                            accessToken: ""
                            ))
                    }
                    
                    onTransfersFetched(activeTransfers)
                }
            })
    }
    
    /*
     Add a new transfer to the put.io transfers.
     */
    func addTransfer(_ downloadURL: String, onTransferAdded: @escaping () -> Void, parentId: Int?) {
        guard let accessToken = UserDefaults.standard.value(forKey: "accessToken") as? String else {
            print("Access token is missing")
            return
        }
        
        // Define where the new transfer should be stored.
        var saveParentId = 0
        if let parentId = parentId {
            saveParentId = parentId
        }
        
        Alamofire.request(
            getAddApiURL(accessToken),
            method: .post,
            parameters: [
                "url": downloadURL,
                "save_parent_id": saveParentId,
                "extract": true
            ]
            )
            .response(completionHandler: { (response) in
                if response.response?.statusCode == 200 {
                    print("Transfer added successfully")
                    onTransferAdded()
                } else {
                    print("Failed with error: \(response.error)")
                    print(response.data)
                    print(String(describing: response.response))
                }
            })
    }
    
    /*
     Return the download progress.
     */
    func getDownloadProgress(_ file: File) -> Float {
        if let download = downloads[file.getId()] {
            return download["downloadProgress"] as! Float
        }
        return 0.0
    }
    
    /*
     Return a boolean determining whether a download task for a given file
     is in progress or not.
     */
    func isTaskRunning(_ file: File) -> Bool {
        if let download = downloads[file.getId()] {
            if let isTaskRunning = download["taskRunning"] as? Bool {
                return isTaskRunning
            }
        }
        return false
    }
    
    /*
     Get the API url necessary for fetching a file/directory from put.io.
     */
    fileprivate func getApiURL (_ parent: File?, accessToken: String) -> URL {
        if parent != nil {
            return URL(string: "https://api.put.io/v2/files/list?oauth_token=\(accessToken)&parent_id=\(parent!.getId())")!
        } else {
            return URL(string: "https://api.put.io/v2/files/list?oauth_token=\(accessToken)")!
        }
    }
    
    /*
     Get the API url for deleting a file in put.io.
     */
    fileprivate func getDeleteApiURL (_ accessToken: String) -> URL {
        return URL(string: "https://api.put.io/v2/files/delete?oauth_token=\(accessToken)")!
    }
    
    /*
     Get the API url for adding a new transfer.
     */
    fileprivate func getAddApiURL (_ accessToken: String) -> URL {
        return URL(string: "https://api.put.io/v2/transfers/add?oauth_token=\(accessToken)")!
    }
    
    /*
     Get the API url for adding a new transfer.
     */
    fileprivate func getActiveTransfersApiURL (_ accessToken: String) -> URL {
        return URL(string: "https://api.put.io/v2/transfers/list?oauth_token=\(accessToken)")!
    }
}
